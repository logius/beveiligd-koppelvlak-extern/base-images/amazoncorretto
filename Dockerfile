﻿FROM amazoncorretto:17-alpine3.18-jdk

#See https://hub.docker.com/_/amazoncorretto
ARG ALPINE_VERSION=v3.18
#See https://github.com/open-telemetry/opentelemetry-java-instrumentation/releases/
ARG OPEN_TELEMETRY_JAVA_AGENT_VERSION=v1.32.0
ARG OPEN_TELEMETRY_JAVA_AGENT=opentelemetry-javaagent.jar

RUN mkdir -p /opt/app
WORKDIR /opt/app

USER root

RUN apk add \
      --no-cache \
      --repository http://dl-cdn.alpinelinux.org/alpine/${ALPINE_VERSION}/main \
      ca-certificates tzdata curl
RUN getIt() { curl -kLS --retry-all-errors --retry 10 --retry-delay 1 ${1} -o ${2}; } && \
  getIt https://github.com/open-telemetry/opentelemetry-java-instrumentation/releases/download/${OPEN_TELEMETRY_JAVA_AGENT_VERSION}/${OPEN_TELEMETRY_JAVA_AGENT} /opt/app/opentelemetry-javaagent.jar

COPY config .

RUN apk del \
      --no-cache \
      --repository http://dl-cdn.alpinelinux.org/alpine/${ALPINE_VERSION}/main \
      curl

ENV TZ=UTC
